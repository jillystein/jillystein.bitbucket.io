$("#flipEntry").flip({trigger:'manual', speed:350});

var textContent = $('textarea').val();

function checkInput() {
	setInterval(function(){ 
		textContent = $('textarea').val();
		if (textContent) {
			$('.placeholder-container').hide()
            $('.confirm').css({opacity: 1});
		} else {
			$('.placeholder-container').show()
            $('.confirm').css({opacity: 0});
		}
	}, 30);
}

checkInput();

function myFunction() {
  var x = document.getElementById('.myInput').value;
  document.getElementById('.btn-animated').innerHTML = "You wrote: " + x;
}

var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        // debugger;
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };


//// Terrible hacky code

    function initializeTypewrite2(txt) {
        // debugger;
        // var texty = txt;
        console.log('ehllo')
        var elements = document.getElementsByClassName('typewrite2');
        for (var i=0; i<elements.length; i++) {
            // var toRotate = texty;
            var period = elements[i].getAttribute('data-period');
            if (txt) {
                new TxtType2(elements[i], JSON.parse(txt), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite2 > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };

    var TxtType2 = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType2.prototype.tick = function() {
        // debugger
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

/// End of terrible hacky code

 var nameList= ['mark','donna','elana','steve'];

 $('.confirm').click(
	function(e){
        if (textContent) {
		e.preventDefault();
		$("#flipEntry").flip(true);
        showCustomPage(textContent)
	 }
    }
);


function showCustomPage(name) {
    name = name.toLowerCase();

    console.log(nameObj[name].text)
    initializeTypewrite2(nameObj[name].text)
    $("#personalEntryPic").attr("src",nameObj[name].image);
}

// make all same dimensions
// shorten intros that are too long
// speed up text
// cut out all whitespace in pics (josh, dad)

var nameObj = {
    katie: {
        text: `["Hi shmatie I love you more than anything!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964643/all_stars/images/name/katie/0_m0v1wy.png"
    },
    donna: {
        text: `["Hi mommala I love you and can't wait to be your roomie!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964617/all_stars/images/name/donna/0_mcivme.png"
    },
    adam: {
        text: `["Hi hazza I miss you already!!!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964642/all_stars/images/name/adam/0_qcr0x3.png"
    },
    bec: {
        text: `["Hi beccy I miss you already!!!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964618/all_stars/images/name/bec/0_kybykg.png"
    },
    brett: {
        text: `["Hi hbr I miss you already!!!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964615/all_stars/images/name/brett/0_cpsscr.png"
    },
    calvin: {
        text: `["Hi cal I miss you already my sista"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964631/all_stars/images/name/calvin/0_g21rcg.png"
    },
    elana: {
        text: `["Hi musky you're my best friend in the world"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964620/all_stars/images/name/elana/0_z2bhqx.png"
    },
    haley: {
        text: `["Hi sis see you soon neighbor!!!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964623/all_stars/images/name/haley/0_fcgnws.png"
    },
    ian: {
        text: `["Hi haze I miss you already!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964616/all_stars/images/name/ian/0_xkxah4.png"
    },
    jason: {
        text: `["Hi silv I miss you already!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964634/all_stars/images/name/jason/0_ydzqgq.png"
    },
    mark: {
        text: `["Hi daddala I love you and can't wait to be your roomie!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964626/all_stars/images/name/mark/0_vfxcqg.png"
    },
    mikey: {
        text: `["Hi mikey b nyc see you soon best pal!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964636/all_stars/images/name/mikey/0_iftqjd.png"
    },
    mila: {
        text: `["Hi my sister I love you forever and ever!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964637/all_stars/images/name/mila/0_ebvmyp.png"
    },
    steven: {
        text: `["Hi stevie I love you most and I miss you already!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964628/all_stars/images/name/steve/0_rie7fs.png"
    },
    zach: {
        text: `["Hi pookie see you soon roomie!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964633/all_stars/images/name/zach/0_tlr9h5.png"
    },
    jamie: {
        text: `["Hi jaminé I missed you so much and love you even more!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964613/all_stars/images/name/jamie/0_huf90b.png"
    },
    jordan: {
        text: `["Hi goober wanna move to new york together???"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964647/all_stars/images/name/jordan/0_c4kjjt.png"
    },
    josh: {
        text: `["Hi joj see you real life soon for lil dots nyc!!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964645/all_stars/images/name/josh/0_vae95b.png"
    },
    maddi: {
        text: `["Hi wad I'm never leaving you again my best friend I love you!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964629/all_stars/images/name/maddi/0_zhierz.png"
    },
    mariel: {
        text: `["Hi murhla I missed you so much I can't wait to see you!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964638/all_stars/images/name/mariel/0_i6dsza.png"
    },
    volpert: {
        text: `["Hi shmolpy I can't wait to see your perfect angel face!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964621/all_stars/images/name/volp/0_zi89sx.png"
    },
    oliver: {
        text: `["Hi bogust I miss you already!!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964650/all_stars/images/name/oliver/0_dptjrq.png"
    },
    nrob: {
        text: `["Hi enny I missed you so much you perfect human being"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964640/all_stars/images/name/nrob/0_xyzfxr.png"
    },
    valner: {
        text: `["Hi val thanks for sending me off on my lil adventure my momma!!"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964648/all_stars/images/name/val/0_hdb4q2.png"
    },
    sophia: {
        text: `["Hi my dina soul sister boyfriend we have 1 million hours of saturday mornings to catch up on"]`,
        image: "https://res.cloudinary.com/jillystein/image/upload/q_5/v1563964624/all_stars/images/name/sophia/0_afjlv3.png"
    },
    
}



















